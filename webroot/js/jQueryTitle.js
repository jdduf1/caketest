$(function() 
{
//we need to create a field with an id of date_published
$("#date_published").datepicker
(
{
minDate: "-15y",
maxDate: "+1y",
dateFormat: "dd/mm/yy",
showOn: "button",
//to display a calendar image, place this image in webroot/img
buttonImage: "<?php echo $this->getAttribute('Webroot').'img/calendar.gif';?>",
buttonImageOnly: true,
buttonText: 'Show',
changeMonth: true,
changeYear: true
});
});
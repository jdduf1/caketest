<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Title Entity
 *
 * @property string $ISBN
 * @property int $publisher_id
 * @property string $title
 * @property \Cake\I18n\FrozenDate $date_published
 * @property string $description
 * @property string $topic
 * @property string $comments
 * @property float $cost_price
 * @property float $retail_price
 * @property int $no_in_stock
 *
 * @property \App\Model\Entity\Publisher $publisher
 */
class Title extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'publisher_id' => true,
        'title' => true,
        'date_published' => true,
        'description' => true,
        'topic' => true,
        'comments' => true,
        'cost_price' => true,
        'retail_price' => true,
        'no_in_stock' => true,
        'publisher' => true
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Publisher Entity
 *
 * @property int $ID
 * @property string $company_name
 * @property string $contact_fname
 * @property string $contact_sname
 * @property string $pub_street
 * @property string $pub_suburb
 * @property string $pub_state
 * @property string $pub_pc
 * @property string $phone
 * @property string $email
 *
 * @property \App\Model\Entity\Title[] $titles
 */
class Publisher extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_name' => true,
        'contact_fname' => true,
        'contact_sname' => true,
        'pub_street' => true,
        'pub_suburb' => true,
        'pub_state' => true,
        'pub_pc' => true,
        'phone' => true,
        'email' => true,
        'titles' => true
    ];

    protected function _getFullName()
    {
        return $this->contact_sname . ', ' . $this->contact_fname;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Titles Model
 *
 * @property \App\Model\Table\PublishersTable|\Cake\ORM\Association\BelongsTo $Publishers
 *
 * @method \App\Model\Entity\Title get($primaryKey, $options = [])
 * @method \App\Model\Entity\Title newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Title[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Title|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Title patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Title[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Title findOrCreate($search, callable $callback = null, $options = [])
 */
class TitlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('titles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('ISBN');

        $this->belongsTo('Publishers', [
            'foreignKey' => 'publisher_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('ISBN')
            ->allowEmpty('ISBN', 'create');

        $validator
            ->scalar('title')
            ->requirePresence('title', 'create')
            ->notEmpty('title', 'Please enter a value for the Title field');

        $validator
            ->date('date_published')
            ->requirePresence('date_published', 'create')
            ->notEmpty('date_published');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('topic')
            ->allowEmpty('topic');

        $validator
            ->scalar('comments')
            ->allowEmpty('comments');

        $validator
            ->decimal('cost_price')
            ->requirePresence('cost_price', 'create')
            ->notEmpty('cost_price');

        $validator
            ->decimal('retail_price')
            ->requirePresence('retail_price', 'create')
            ->notEmpty('retail_price');

        $validator
            ->integer('no_in_stock')
            ->allowEmpty('no_in_stock');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['publisher_id'], 'Publishers'));

        return $rules;
    }


    public function beforeMarshal(Event $event, ArrayObject $data, 
    ArrayObject $options)
    {
    if($data['date_published'] != '')
    {
    $data['date_published'] = 
    $this->dateFormatBeforeSave($data['date_published']);
    }
    }
    function dateFormatBeforeSave($dateString)
    {
    $newDate = explode("/",$dateString);
    return $newDate[2]."-".$newDate[1]."-".$newDate[0];
    //date is now in yyyy-mm-dd format which MySQL likes
    }
}

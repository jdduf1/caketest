<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title[]|\Cake\Collection\CollectionInterface $titles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Title'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Publishers'), ['controller' => 'Publishers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Publisher'), ['controller' => 'Publishers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="titles index large-9 medium-8 columns content">
    <h3><?= __('Titles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ISBN') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publisher_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_published') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('topic') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cost_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('retail_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('no_in_stock') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($titles as $title): ?>
            <tr>
                <td><?= h($title->ISBN) ?></td>
                <td><?= $title->has('publisher') ? $this->Html->link($title->publisher->company_name, ['controller' => 'Publishers', 'action' => 'view', $title->publisher->ID]) : '' ?></td>
                <td><?= h($title->title) ?></td>
                <td><?= h($title->date_published) ?></td>
                <td><?= h($title->description) ?></td>
                <td><?= h($title->topic) ?></td>
                <td><?= h($title->comments) ?></td>
                <td><?= $this->Number->format($title->cost_price) ?></td>
                <td><?= $this->Number->format($title->retail_price) ?></td>
                <td><?= $this->Number->format($title->no_in_stock) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $title->ISBN]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $title->ISBN]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $title->ISBN], ['confirm' => __('Are you sure you want to delete # {0}?', $title->ISBN)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Titles'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Publishers'), ['controller' => 'Publishers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Publisher'), ['controller' => 'Publishers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="titles form large-9 medium-8 columns content">
    <?= $this->Form->create($title) ?>
    <fieldset>
        <legend><?= __('Add Title') ?></legend>
        <?php
            echo $this->Form->control('publisher_id', ['options' => $publishers]);
            echo $this->Form->control('title');
            echo $this->Form->control('date_published');
            echo $this->Form->control('description');
            echo $this->Form->control('topic');
            echo $this->Form->control('comments');
            echo $this->Form->control('cost_price');
            echo $this->Form->control('retail_price');
            echo $this->Form->control('no_in_stock');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

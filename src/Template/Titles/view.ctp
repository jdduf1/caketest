<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title $title
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Title'), ['action' => 'edit', $title->ISBN]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Title'), ['action' => 'delete', $title->ISBN], ['confirm' => __('Are you sure you want to delete # {0}?', $title->ISBN)]) ?> </li>
        <li><?= $this->Html->link(__('List Titles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Title'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Publishers'), ['controller' => 'Publishers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publisher'), ['controller' => 'Publishers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="titles view large-9 medium-8 columns content">
    <h3><?= h($title->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ISBN') ?></th>
            <td><?= h($title->ISBN) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Publisher') ?></th>
            <td><?= $title->has('publisher') ? $this->Html->link($title->publisher->ID, ['controller' => 'Publishers', 'action' => 'view', $title->publisher->ID]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($title->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($title->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Topic') ?></th>
            <td><?= h($title->topic) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comments') ?></th>
            <td><?= h($title->comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cost Price') ?></th>
            <td><?= $this->Number->format($title->cost_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Retail Price') ?></th>
            <td><?= $this->Number->format($title->retail_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No In Stock') ?></th>
            <td><?= $this->Number->format($title->no_in_stock) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Published') ?></th>
            <td><?= h($title->date_published) ?></td>
        </tr>
    </table>
</div>

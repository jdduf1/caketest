<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publisher $publisher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Publisher'), ['action' => 'edit', $publisher->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Publisher'), ['action' => 'delete', $publisher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $publisher->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Publishers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publisher'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Titles'), ['controller' => 'Titles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Title'), ['controller' => 'Titles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="publishers view large-9 medium-8 columns content">
    <h3><?= h($publisher->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company Name') ?></th>
            <td><?= h($publisher->company_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Fname') ?></th>
            <td><?= h($publisher->contact_fname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact Sname') ?></th>
            <td><?= h($publisher->contact_sname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pub Street') ?></th>
            <td><?= h($publisher->pub_street) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pub Suburb') ?></th>
            <td><?= h($publisher->pub_suburb) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pub State') ?></th>
            <td><?= h($publisher->pub_state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pub Pc') ?></th>
            <td><?= h($publisher->pub_pc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($publisher->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($publisher->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($publisher->ID) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Titles') ?></h4>
        <?php if (!empty($publisher->titles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('ISBN') ?></th>
                <th scope="col"><?= __('Publisher Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Date Published') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Topic') ?></th>
                <th scope="col"><?= __('Comments') ?></th>
                <th scope="col"><?= __('Cost Price') ?></th>
                <th scope="col"><?= __('Retail Price') ?></th>
                <th scope="col"><?= __('No In Stock') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($publisher->titles as $titles): ?>
            <tr>
                <td><?= h($titles->ISBN) ?></td>
                <td><?= h($titles->publisher_id) ?></td>
                <td><?= h($titles->title) ?></td>
                <td><?= h($titles->date_published) ?></td>
                <td><?= h($titles->description) ?></td>
                <td><?= h($titles->topic) ?></td>
                <td><?= h($titles->comments) ?></td>
                <td><?= h($titles->cost_price) ?></td>
                <td><?= h($titles->retail_price) ?></td>
                <td><?= h($titles->no_in_stock) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Titles', 'action' => 'view', $titles->ISBN]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Titles', 'action' => 'edit', $titles->ISBN]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Titles', 'action' => 'delete', $titles->ISBN], ['confirm' => __('Are you sure you want to delete # {0}?', $titles->ISBN)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

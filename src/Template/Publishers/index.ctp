<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publisher[]|\Cake\Collection\CollectionInterface $publishers
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Publisher'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Titles'), ['controller' => 'Titles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Title'), ['controller' => 'Titles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="publishers index large-9 medium-8 columns content">
    <h3><?= __('Publishers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_fname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_sname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pub_street') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pub_suburb') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pub_state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pub_pc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($publishers as $publisher): ?>
            <tr>
                <td><?= $this->Number->format($publisher->ID) ?></td>
                <td><?= h($publisher->company_name) ?></td>
                <td><?= h($publisher->full_name) ?></td>
                <td><?= h($publisher->pub_street) ?></td>
                <td><?= h($publisher->pub_suburb) ?></td>
                <td><?= h($publisher->pub_state) ?></td>
                <td><?= h($publisher->pub_pc) ?></td>
                <td><?= h($publisher->phone) ?></td>
                <td><?= h($publisher->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $publisher->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $publisher->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $publisher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $publisher->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
